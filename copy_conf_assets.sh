#!/bin/bash
#relies on a mounted drive at /usr/local/conf/tomcat pointing to a local folder of conf files

#copy context.xml to conf tomcat folder
yes | cp -rf /usr/local/conf/tomcat/context.xml $CATALINA_HOME/conf/context.xml

#copy DigitalWare.war to webapps tomcat folder
yes | cp -rf /usr/local/conf/tomcat/DigitalWare.war $CATALINA_HOME/webapps/DigitalWare.war
yes | rm -rf $CATALINA_HOME/webapps/DigitalWare
